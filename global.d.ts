type ResDataType = {
  code: string;
  name: string;
  company_code: string;
  domain: string;
  email: string;
  phone: string;
  address_code: string;
  create_date: string;
  create_user: string;
  update_date: string;
  update_user: string;
  img_url: string;
  min_order_amount: number;
  shipping_list: [
    {
      code: string;
      restaurant_code: string;
      name: string;
      isactive: boolean;
    },
    {
      code: string;
      restaurant_code: string;
      name: string;
      isactive: boolean;
    }
  ];
  payment_method_list: [
    {
      code: string;
      restaurant_code: string;
      name: string;
      isactive: boolean;
    },
    {
      code: string;
      restaurant_code: string;
      name: string;
      isactive: boolean;
    }
  ];
};

type productDataType = {
  code: string;
  name: string;
  name_en: string;
  category_code: string;
  type: string;
  descrition: "" | null;
  img_url: string;
  price: number;
  price_code: "" | null;
  restaurant_code: string;
  create_date: "" | null;
  create_user: "" | null;
  update_date: "" | null;
  update_user: string;
};

type categoryDataType = {
  code: string;
  name: string;
  description: string;
  create_date: string | null;
  create_user: string | null;
  update_date: string | null;
  update_user: string | null;
  restaurant_code: string;
  img_url: string;
};
