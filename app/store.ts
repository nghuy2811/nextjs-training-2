import { configureStore } from "@reduxjs/toolkit";

import productsSlice from "../features/products/productsSlice";

const store = configureStore({
  reducer: {
    products: productsSlice,
  },
});

export type AppState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export default store;
