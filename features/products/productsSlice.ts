import { createSelector, createSlice } from "@reduxjs/toolkit";

import { AppState } from "../../app/store";

export type productState = {
  [key: string]: {
    code: string;
    quantity: number;
    name: string;
    price: number;
    totalPrice: number;
    img_url: string;
  };
};

const initialState: productState = {};

const updateTotalPrice = (state: productState, code: string) => {
  state[code].totalPrice = state[code].quantity * state[code].price;
};

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    addNewProduct(state, action) {
      const code = action.payload.code;
      if (state.hasOwnProperty(action.payload.code)) {
        state[code].quantity++;
        updateTotalPrice(state, code);
      } else {
        state[code] = action.payload;
        state[code].quantity = 1;
        updateTotalPrice(state, code);
      }
    },
    increaseQuantity(state, action) {
      const code = action.payload.code;
      state[code].quantity++;
      updateTotalPrice(state, code);
    },
    decreaseQuantity(state, action) {
      const code = action.payload.code;
      state[code].quantity--;
      updateTotalPrice(state, code);
    },
    deleteProduct(state, action) {
      const code = action.payload;
      delete state[code];
    },
  },
});

export const {
  addNewProduct,
  increaseQuantity,
  decreaseQuantity,
  deleteProduct,
} = productsSlice.actions;

export default productsSlice.reducer;

const selectAllProducts = (state: AppState) => state.products;

export const selectAllProductsArray = createSelector(
  (state: AppState) => state.products,
  (products) => {
    return Object.values(products);
  }
);

export const selectAllProductCodes = createSelector(
  selectAllProductsArray,
  (products) => {
    return products.map((product) => product.code);
  }
);

export const selectProductByCode = (code: string) =>
  createSelector(selectAllProducts, (products) => products[code]);
