import App from "next/app";
import type { AppProps, AppContext } from "next/app";
import axios from "axios";
import { Provider } from "react-redux";

import store from "../app/store";
import Layout from "../components/layout/Layout";
import "../styles/app.scss";

type AppPropsType = {
  Component: any;
  pageProps: AppProps;
  data: ResDataType[];
};

function MyApp({ Component, pageProps, data }: AppPropsType) {
  return (
    <Layout data={data}>
      <Provider store={store}>
        <Component data={data} {...pageProps} />
      </Provider>
    </Layout>
  );
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext);
  const data = await (
    await axios.get("http://54.93.83.177:80/sys_restaurant/select?code=RES0001")
  ).data;

  return { ...appProps, data };
};

export default MyApp;
