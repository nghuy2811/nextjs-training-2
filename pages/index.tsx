import axios from "axios";

import ShoppingCart from "../components/shopping-cart/ShoppingCart";
import Banner from "../components/banner/Banner";
import MainProducts from "../components/main-products/MainProducts";

type homeProps = {
  data: ResDataType[];
  categories: categoryDataType[];
  products: productDataType[];
};

const Home = ({ data, products, categories }: homeProps) => {
  return (
    <>
      <ShoppingCart minOrder={data[0].min_order_amount} />
      <Banner
        restaurantName={data[0].name}
        addressCode={data[0]["address_code"]}
        minOrderAmount={data[0]["min_order_amount"]}
      />
      <MainProducts categories={categories} products={products} />
    </>
  );
};

export const getServerSideProps = async () => {
  const categoryRes = await axios.get(
    "http://54.93.83.177:80/md_category/selectall?restaurant_code=RES0001"
  );
  const productRes = await axios.get(
    "http://54.93.83.177:80/md_product/selectall?restaurant_code=RES0001"
  );
  const categories: categoryDataType[] = categoryRes.data;
  const products: productDataType[] = productRes.data;

  return {
    props: {
      categories,
      products,
    },
  };
};

export default Home;
