import React from "react";

import styles from "./Footer.module.scss";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <h4 className={styles["footer-text"]}>
        Copyright © 2020 Vietpho. All Rights Reserved.
      </h4>
    </footer>
  );
};

export default Footer;
