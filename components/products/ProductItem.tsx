import React from "react";
import { FaInfoCircle, FaCartPlus } from "react-icons/fa";

import { useAppDispatch } from "../../app/hooks";
import { addNewProduct } from "../../features/products/productsSlice";
import styles from "./style.module.scss";

type ProductItemProp = {
  product: productDataType;
};

const ProductItem = ({ product }: ProductItemProp) => {
  const dispatch = useAppDispatch();

  const handleAddProduct = (
    code: string,
    name: string,
    price: number,
    imgUrl: string
  ) => {
    const data = {
      code,
      name,
      price,
      img_url: imgUrl,
    };
    dispatch(addNewProduct(data));
  };

  return (
    <div
      key={product.code}
      className={
        styles["product-item"] +
        " w-full xxl:w-[calc(50%-20px)] mx-[10px] mb-[10px] xxl:mt[10px] border-2 border-solid border-[#f3f3f3] hover:border-red"
      }
    >
      <div className="flex py-[10px] px-[20px]">
        <img
          className="w-[120px] max-h-[120px] xxl:max-h-[none]"
          src={`http://54.93.83.177:80/${product.img_url}`}
          alt="Product image"
        />
        <div
          className={`${styles["product-heading"]} flex-1 py-[10px] px-[20px] font-bold text-navy hover:text-red text-[20px] leading-[30px] cursor-pointer`}
        >
          <div className="mb-[5px]">
            <span>{product.name} </span>
            <span className="inline-block text-[12px]">
              <FaInfoCircle />
            </span>
          </div>
          <div className="text-red mt-[20px] text-[18px] leading-[27px] xxl:text-[20px] xxl:leading-[30px] font-bold">
            {product.price} €
          </div>
        </div>
        <span
          className={`${styles["add-to-cart"]} cursor-pointer inline-block text-[35px] xxl:text-[40px] self-center text-[#167f92] hover:text-red`}
          onClick={() =>
            handleAddProduct(
              product.code,
              product.name,
              product.price,
              product.img_url
            )
          }
        >
          <FaCartPlus />
        </span>
      </div>
    </div>
  );
};

export default ProductItem;
