import React, { createRef, useEffect, useCallback } from "react";

import { ProductsByCategory } from "../main-products/MainProducts";
import ProductItem from "./ProductItem";

type productsProps = {
  categories: categoryDataType[];
  productsByCategory: ProductsByCategory;
  categoryInView: string | undefined;
  activeCategory: string | undefined;
  onScrollCallback: (code: string) => void;
};

const Products = ({
  categories,
  productsByCategory,
  categoryInView,
  activeCategory,
  onScrollCallback,
}: productsProps) => {
  const categoriesHaveRefs = categories.map((category) => {
    return {
      ...category,
      ref: createRef<HTMLDivElement>(),
    };
  });

  const handleCategoryInView = useCallback(() => {
    let target;
    if (categoryInView) {
      target = categoriesHaveRefs
        .map((category) => category.ref)
        .filter((ref) => {
          if (ref.current) {
            const categoryCode = ref.current.getAttribute("data-category");
            if (categoryCode === categoryInView) {
              return ref;
            }
          }
        });
    }

    if (target) {
      target[0].current?.scrollIntoView();
    }
  }, [categoriesHaveRefs, categoryInView]);

  const handleScroll = useCallback(() => {
    categoriesHaveRefs.forEach((category) => {
      if (category.ref.current) {
        const rect = category.ref.current.getBoundingClientRect();

        let elemTop = rect.top;
        let elemBottom = rect.bottom;

        if (elemTop < 65 && elemBottom > 65) {
          onScrollCallback(category.code);
        }
      }
    });
  }, [activeCategory]);

  useEffect(() => {
    handleCategoryInView();
  }, [categoryInView]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [activeCategory]);

  return (
    <div className="my-[30px]">
      <div className="container">
        {categoriesHaveRefs.map((category) => (
          <div
            ref={category.ref}
            className="mb-5 scroll-mt-[65px]"
            key={category.code}
            data-category={category.code}
          >
            <div className="mb-[10px]">
              <div
                className="h-[140px] bg-cover bg-no-repeat bg-center w-full rounded-[4px]"
                style={{
                  backgroundImage: `url(http://54.93.83.177:80/${category.img_url}`,
                }}
              ></div>
              <div className="p-4 text-navy font-bold text-[22px] leading-[33px] bg-[#f3f3f3]">
                {category.name}
              </div>
            </div>
            <div className="flex flex-wrap w-full">
              {productsByCategory[`${category.code}`].map((product) => (
                <ProductItem product={product} key={product.code} />
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Products;
