import React from "react";
import { IconContext } from "react-icons";
import { FaPhoneAlt, FaEnvelope } from "react-icons/fa";

import LanguageBox from "./LanguageBox";
import styles from "./Header.module.scss";

type HeaderType = {
  data: ResDataType[];
};

const Header = ({ data }: HeaderType) => {
  return (
    <header className={styles["header"]}>
      <div className={styles["header-wrapper"]}>
        <div className="container">
          <div className={styles["header-content"]}>
            <img
              src={`http://54.93.83.177:80/${data[0].img_url}`}
              alt="Logo"
              className={styles["logo"]}
            />
            <div className={styles["header-col-right"]}>
              <LanguageBox className={styles["language-box"]} />
              <div className={styles["header-info"]}>
                <div className={styles["header-info-row"]}>
                  <IconContext.Provider
                    value={{ className: `${styles["header-info-icon"]}` }}
                  >
                    <FaPhoneAlt />
                  </IconContext.Provider>
                  <span className={styles["header-info-text"]}>
                    Call us: {data[0].phone}
                  </span>
                </div>
                <div className={styles["header-info-row"]}>
                  <IconContext.Provider
                    value={{ className: `${styles["header-info-icon"]}` }}
                  >
                    <FaEnvelope />
                  </IconContext.Provider>
                  <span className={styles["header-info-text"]}>
                    Mail: {data[0].email}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
