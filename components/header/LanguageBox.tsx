import React, { useCallback, useEffect, useRef, useState } from "react";

import styles from "./LanguageBox.module.scss";

type LanguageBoxProp = {
  className: string;
};

const LanguageBox = ({ className }: LanguageBoxProp) => {
  const VIBox = useRef<HTMLDivElement>(null);
  const ENBox = useRef<HTMLDivElement>(null);
  const DEBox = useRef<HTMLDivElement>(null);

  const [open, setOpen] = useState(false);
  const [activeBox, setActiveBox] = useState(VIBox.current!);

  return (
    <div
      className={`${className} ${styles["language-box"]} ${
        open ? styles["open"] : ""
      }`}
      onClick={() => setOpen(!open)}
      style={open ? { height: `120px` } : { height: `40px` }}
    >
      <div
        ref={VIBox}
        className={`${styles["lang-box-opt"]} ${
          VIBox.current! === activeBox ? styles["active"] : null
        }`}
        onClick={() => setActiveBox(VIBox.current!)}
      >
        <div className={`${styles["lang-flag"]} ${styles["vi-flag"]}`}></div>
        <span>VI</span>
      </div>
      <div
        ref={ENBox}
        className={`${styles["lang-box-opt"]} ${
          ENBox.current! === activeBox ? styles["active"] : null
        }`}
        onClick={() => setActiveBox(ENBox.current!)}
      >
        <div className={`${styles["lang-flag"]} ${styles["en-flag"]}`}></div>
        <span>EN</span>
      </div>
      <div
        ref={DEBox}
        className={`${styles["lang-box-opt"]} ${
          DEBox.current! === activeBox ? styles["active"] : null
        }`}
        onClick={() => setActiveBox(DEBox.current!)}
      >
        <div className={`${styles["lang-flag"]} ${styles["de-flag"]}`}></div>
        <span>DE</span>
      </div>
    </div>
  );
};

export default LanguageBox;
