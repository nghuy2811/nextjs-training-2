import React, { useState, useRef, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Mousewheel, Navigation } from "swiper";
import "swiper/css";
import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai";

import { categoryProps } from "./Category";
import styles from "./style.module.scss";

const CategorySwiper = ({
  categories,
  callback,
  activeCategory,
}: categoryProps) => {
  const sliderRef = useRef<any>(null);
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    const index = categories.findIndex(
      (category) => category.code === activeCategory
    );

    setActiveIndex(index);
  }, [activeCategory]);

  useEffect(() => {
    sliderRef.current.swiper.activeIndex = activeIndex;

    sliderRef.current.swiper.slideTo(activeIndex);
  }, [activeIndex]);

  return (
    <div className="w-[100%] h-[100%] pl-[85px] flex items-center">
      <div className="relative w-[100%]">
        <div className="mx-[30px]">
          <Swiper
            ref={sliderRef}
            modules={[Navigation]}
            navigation={{
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev",
              disabledClass: `${styles["swiper-button-disabled"]}`,
            }}
            slidesPerView={"auto"}
          >
            {categories.map((category, index) => (
              <SwiperSlide
                key={category.code}
                className={
                  category.code === activeCategory
                    ? "swiper-slide-active selected"
                    : ""
                }
                data-category={category.code}
                onClick={() => {
                  setActiveIndex(index);
                  callback(category.code);
                }}
              >
                <span>{category.name}</span>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
        <button className="absolute top-[50%] left-0 translate-y-[-50%] swiper-button-prev">
          <span className="text-[20px] text-navy">
            <AiOutlineLeft />
          </span>
        </button>
        <button className="absolute top-[50%] right-0 translate-y-[-50%] swiper-button-next">
          <span className="text-[20px] text-navy">
            <AiOutlineRight />
          </span>
        </button>
      </div>
    </div>
  );
};

export default CategorySwiper;
