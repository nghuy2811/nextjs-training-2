import React, { useState, useRef, useCallback, useEffect } from "react";
import { FaSearch } from "react-icons/fa";
import { AiOutlineClose } from "react-icons/ai";

import CategorySwiper from "./CategorySwiper";
import styles from "./style.module.scss";

export type categoryProps = {
  categories: categoryDataType[];
  callback: (code: string) => void;
  activeCategory: string | undefined;
};

const Category = ({ categories, callback, activeCategory }: categoryProps) => {
  const navRef = useRef<HTMLDivElement>(null);
  const searchInputRef = useRef<HTMLInputElement>(null);
  const [navFixed, setNavFixed] = useState(false);
  const [openSearchBox, setOpenSearchBox] = useState(false);

  const handleOpenSearchBar = useCallback(() => {
    setOpenSearchBox(true);

    if (searchInputRef.current) {
      searchInputRef.current.focus();
    }
  }, [openSearchBox]);

  useEffect(() => {
    const navOffsetTop = navRef.current?.getBoundingClientRect().top;
    const navOffsetBottom = navRef.current?.getBoundingClientRect().bottom;

    const handleScroll = () => {
      const windowY = window.scrollY;

      if (navOffsetTop && windowY) {
        if (windowY > navOffsetTop) {
          setNavFixed(true);
        } else setNavFixed(false);
      }
    };

    handleScroll();

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div
      ref={navRef}
      className={`${styles["category"]} ${navFixed ? styles.fixed : ""}`}
    >
      <div className="container">
        <div className="relative w-[100%] h-[100%] flex items-center">
          <div
            className={` ${styles["search-box"]} ${
              openSearchBox ? styles.open : ""
            }`}
          >
            <span
              className={styles["search-icon"]}
              onClick={handleOpenSearchBar}
            >
              <FaSearch />
            </span>
            <input
              ref={searchInputRef}
              type="text"
              className={styles["search-input"]}
              placeholder="Search for meals..."
            />
            <span
              className={styles["search-close-btn"]}
              onClick={() => setOpenSearchBox(false)}
            >
              <AiOutlineClose />
            </span>
          </div>

          <CategorySwiper
            categories={categories}
            activeCategory={activeCategory}
            callback={callback}
          />
        </div>
      </div>
    </div>
  );
};

export default Category;
