import React, { ReactElement } from "react";

import Header from "../header/Header";
import Footer from "../footer/Footer";

interface LayoutProps {
  data: ResDataType[];
  children: ReactElement;
}

const Layout = ({ data, children }: LayoutProps) => {
  return (
    <>
      <Header data={data} />
      <main>{children}</main>
      <Footer />
    </>
  );
};

export default Layout;
