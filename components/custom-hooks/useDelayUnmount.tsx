import { useEffect, useRef, useState } from "react";

function useDelayUnmount(status: boolean, changeTime: number) {
  const didMount = useRef(false);
  const [show, setShow] = useState(status);

  useEffect(() => {
    if (didMount.current) {
      if (show === true) {
        setTimeout(() => {
          setShow(status);
        }, changeTime);
      } else {
        setShow(status);
      }
    } else {
      didMount.current = true;
    }
  }, [status]);

  return show;
}

export default useDelayUnmount;
