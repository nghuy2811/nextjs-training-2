import React, { useCallback, useState } from "react";
import { IconContext } from "react-icons";
import { FaInfoCircle } from "react-icons/fa";

import AboutUs from "../about-us/AboutUs";
import styles from "./Banner.module.scss";

type BannerProps = {
  restaurantName: string;
  addressCode: string;
  minOrderAmount: number;
};

const Banner = ({
  restaurantName,
  addressCode,
  minOrderAmount,
}: BannerProps) => {
  const [openInfo, setOpenInfo] = useState(false);

  const handleCloseInfo = useCallback(() => {
    setOpenInfo(!openInfo);
  }, [openInfo]);

  return (
    <section
      className={
        styles.banner +
        " relative flex pt-[20px] pb-[28px] justify-center items-center text-center h-[200px]"
      }
    >
      <div className={styles["banner-img"] + " z-[-1]"}></div>
      <div>
        <h1
          className="m-0 mb-4 flex justify-center items-center"
          onClick={() => setOpenInfo(!openInfo)}
        >
          <span
            className={
              styles["banner-heading"] +
              " text-[30px] leading-[42px] lg:text-[35px] lg:leading-[49px] text-white uppercase relative"
            }
          >
            {restaurantName}
            <span className="inline-block ml-2">
              <IconContext.Provider
                value={{ className: styles["heading-icon"] }}
              >
                <FaInfoCircle />
              </IconContext.Provider>
            </span>
          </span>
        </h1>
        <p className="m-0 text-[18px] leading-[27px]  lg:text-[20px] lg:leading-[30px] text-white">
          {addressCode}
        </p>
      </div>
      <AboutUs
        isOpen={openInfo}
        onCloseInfo={handleCloseInfo}
        minOrderAmount={minOrderAmount}
      />
    </section>
  );
};

export default Banner;
