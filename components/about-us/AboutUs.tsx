import React from "react";
import {
  AiOutlineClose,
  AiFillClockCircle,
  AiTwotoneShop,
  AiFillCreditCard,
} from "react-icons/ai";
import { FaBiking, FaBuilding } from "react-icons/fa";

import useDelayUnmount from "../custom-hooks/useDelayUnmount";
import styles from "./AboutUs.module.scss";

type PropsType = {
  isOpen: boolean;
  onCloseInfo: () => void;
  minOrderAmount: number;
};

const AboutUs = ({ isOpen, onCloseInfo, minOrderAmount }: PropsType) => {
  const deliveryTimes = {
    title: "Delivery times",
    icon: <AiFillClockCircle />,
    content: [
      { day: "Monday", time: "Closed for delivery" },
      { day: "Tuesday", time: "12:00 - 21:30" },
      { day: "Wednesday", time: "12:00 - 21:30" },
      { day: "Thursday", time: "12:00 - 21:30" },
      { day: "Friday", time: "12:00 - 21:30" },
      { day: "Saturday", time: "14:00 - 21:30" },
      { day: "Sunday", time: "14:00 - 21:30" },
    ],
  };
  const deliveryCosts = {
    title: "Delivery costs",
    icon: <FaBiking />,
    content: [
      {
        key: "Minimum order amount",
        value: `${minOrderAmount} €`,
      },
      {
        key: "Delivery costs",
        value: "2.00 €",
      },
    ],
  };
  const website = {
    title: "Website",
    icon: <AiTwotoneShop />,
    content: "vietpho.de",
  };
  const paymentMethods = {
    title: "Payment methods",
    icon: <AiFillCreditCard />,
    content: ["/info/cash-money.png", "/info/card.jpg"],
  };
  const colophon = {
    title: "Colophon",
    icon: <FaBuilding />,
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint reiciendis doloribus, rem autem nulla ab unde commodi in harum.",
  };

  const isShow = useDelayUnmount(isOpen, 200);

  const animationStyle = isOpen
    ? { animation: "fadeIn 200ms ease-in" }
    : { animation: "fadeOut 200ms ease-out", animationFillMode: "forwards" };

  return (
    <>
      {isShow && (
        <>
          <div
            className={styles.overlay}
            onClick={onCloseInfo}
            style={animationStyle}
          ></div>
          <div className={styles["popup"]} style={animationStyle}>
            <h3 className="font-bold text-left text-[25px] leading-[25px] text-navy pb-[15px] flex items-center justify-between">
              About Us
              <span
                className="cursor-pointer inline-block text-[20px]"
                onClick={onCloseInfo}
              >
                <AiOutlineClose />
              </span>
            </h3>

            <div className={styles["popup-content"]}>
              <div className={styles["content-row"]}>
                <h4 className={styles["content-row-heading"]}>
                  <span className={styles["content-icon"]}>
                    {deliveryTimes.icon}
                  </span>
                  {deliveryTimes.title}
                </h4>
                <div className={styles["content-row-main"]}>
                  {deliveryTimes.content.map((item) => (
                    <div className={styles["content-row-item"]}>
                      <div className={styles["content-col-left"]}>
                        {item.day}
                      </div>
                      <div className={styles["content-col-right"]}>
                        {item.time}
                      </div>
                    </div>
                  ))}
                </div>
              </div>

              <div className={styles["content-row"]}>
                <h4 className={styles["content-row-heading"]}>
                  <span className={styles["content-icon"]}>
                    {deliveryCosts.icon}
                  </span>
                  {deliveryCosts.title}
                </h4>
                <div className={styles["content-row-main"]}>
                  {deliveryCosts.content.map((item) => (
                    <div className={styles["content-row-item"]}>
                      <div className={styles["content-col-left"]}>
                        {item.key}
                      </div>
                      <div className={styles["content-col-right"]}>
                        {item.value}
                      </div>
                    </div>
                  ))}
                </div>
              </div>

              <div className={styles["content-row"]}>
                <h4 className={styles["content-row-heading"]}>
                  <span className={styles["content-icon"]}>{website.icon}</span>
                  {website.title}
                </h4>
                <div className={styles["content-row-main"]}>
                  {website.content}
                </div>
              </div>

              <div className={styles["content-row"]}>
                <h4 className={styles["content-row-heading"]}>
                  <span className={styles["content-icon"]}>
                    {paymentMethods.icon}
                  </span>
                  {paymentMethods.title}
                </h4>
                <div className={styles["content-row-main"]}>
                  <div className={styles["content-row-card"]}>
                    {paymentMethods.content.map((item, index) => (
                      <div key={index} className={styles["content-row-img"]}>
                        <img src={item} alt="Payment method" />
                      </div>
                    ))}
                  </div>
                </div>
              </div>

              <div className={styles["content-row"]}>
                <h4 className={styles["content-row-heading"]}>
                  <span className={styles["content-icon"]}>
                    {colophon.icon}
                  </span>
                  {colophon.title}
                </h4>
                <div className={styles["content-row-main"]}>
                  {colophon.content}
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default AboutUs;
