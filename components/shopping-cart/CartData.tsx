import React, { useEffect, useState } from "react";

import { useAppSelector } from "../../app/hooks";
import {
  selectAllProductCodes,
  selectAllProductsArray,
} from "../../features/products/productsSlice";
import CartItem from "./CartItem";
import styles from "./style.module.scss";

type CartDataProps = {
  minOrder: number;
  callback: (n: number) => void;
};

const CartData = ({ minOrder, callback }: CartDataProps) => {
  const productCodes = useAppSelector(selectAllProductCodes);
  const products = useAppSelector(selectAllProductsArray);
  const [selectValue, setSelectValue] = useState("initial");

  const subTotal = products.reduce((total, product) => {
    return (total += product.totalPrice);
  }, 0);
  const deliveryCost = selectValue === "delivery" ? 10 : 0;
  const tax = subTotal * 0.1;
  const total = subTotal + deliveryCost + tax;
  const remainingAmount = minOrder - subTotal - tax;

  useEffect(() => {
    callback(total);

    return () => callback(0);
  }, [total]);

  const cartItem = productCodes.map((code) => (
    <CartItem key={code} code={code} />
  ));

  return (
    <div className="flex h-[calc(100%-68px)] flex-col">
      <div className="overflow-scroll pb-[200px] lg:pb-0">
        <ul className="px-[15px] pt-5">{cartItem}</ul>
        <div className="py-[25px] px-[15px] mb-[25px] border-[#e4e4e4] border-b-[1px] border-solid">
          <div className="flex justify-between text-navy text-[16px] leading-[24px] mb-[10px]">
            <span className="w-3/5">Sub-total</span>
            <span className="self-center">{subTotal.toFixed(2)} €</span>
          </div>
          <div className="flex justify-between text-navy text-[16px] leading-[24px] mb-[10px]">
            <span className="w-3/5">Delivery cost</span>
            <span className="self-center">{deliveryCost.toFixed(2)} €</span>
          </div>
          <div className="flex justify-between text-navy text-[16px] leading-[24px] mb-[10px]">
            <span className="w-3/5">Tax amount</span>
            <span className="self-center">{tax.toFixed(2)} €</span>
          </div>
          <div className="flex justify-between text-red text-[16px] leading-[24px] font-bold mb-[10px]">
            <span className="w-3/5">Total</span>
            <span className="self-center">{total.toFixed(2)} €</span>
          </div>
          {selectValue === "delivery" && remainingAmount > 0 && (
            <div className="flex justify-between text-[#0064ff] text-[16px] leading-[24px]">
              <span className="w-3/5">
                Amount needed to reach the minimum order value
              </span>
              <span className="self-center">
                {remainingAmount.toFixed(2)} €
              </span>
            </div>
          )}
        </div>
        {selectValue === "delivery" && (
          <div className="px-[20px] pb-[25px] text-[14px] leading-[21px] text-gray text-center">
            {remainingAmount > 0
              ? `Sorry, you can't checkout yet. We has set a minimum order amount of ${minOrder} € (excl. delivery costs)`
              : `You have reached the minimum order amount of ${minOrder} € to checkout.`}
          </div>
        )}
      </div>
      <div className="fixed left-0 bottom-0 w-full px-[10px] py-[8px] bg-white lg:px-[15px] lg:pb-[20px] lg:relative">
        <div className="mb-[10px]">
          <select
            className={styles["select-shipping"]}
            value={selectValue}
            onChange={(e) => setSelectValue(e.target.value)}
          >
            <option value="initial" disabled>
              Select Shipping Method
            </option>
            <option value="delivery">Delivery</option>
            <option value="pickup">Pickup</option>
          </select>
        </div>
        <button
          className={styles["checkout-btn"]}
          disabled={selectValue === "delivery" && remainingAmount > 0}
        >
          Checkout
        </button>
      </div>
    </div>
  );
};

export default CartData;
