import React, { useCallback, useRef, useState } from "react";
import { BsFillPencilFill, BsFillTrashFill } from "react-icons/bs";

import { useAppSelector, useAppDispatch } from "../../app/hooks";
import {
  selectProductByCode,
  increaseQuantity,
  decreaseQuantity,
  deleteProduct,
} from "../../features/products/productsSlice";
import styles from "./style.module.scss";

type CartItemType = {
  code: string;
};

export type productType = {
  code: string;
  quantity: number;
  name: string;
  price: number;
  totalPrice: number;
  img_url: string;
};

const CartItem = ({ code }: CartItemType) => {
  const product: productType = useAppSelector(selectProductByCode(code));
  const dispatch = useAppDispatch();
  const [noteActive, setNoteActive] = useState(false);
  const [currentText, setCurrentText] = useState("");
  const [note, setNote] = useState("");

  // const handleAddQuantity = useCallback(() => {
  //   const productCode = product.code;

  //   dispatch(increaseQuantity(productCode));
  // }, [code]);

  const handleIncreaseQuantity = () => {
    const data = {
      code: code,
      price: product.price,
    };
    dispatch(increaseQuantity(data));
  };

  const handleDecreaseQuantity = () => {
    const data = {
      code: code,
      price: product.price,
    };
    dispatch(decreaseQuantity(data));
  };

  const handleDeleteProduct = () => {
    dispatch(deleteProduct(code));
  };

  const handleOpenNote = useCallback(() => {
    if (!noteActive) setNoteActive(true);
  }, [noteActive]);

  const handleSaveNote = useCallback(() => {
    setNote(currentText);

    setNoteActive(false);
  }, [currentText]);

  const handleDeleteNote = useCallback(() => {
    setCurrentText("");

    setNote("");

    setNoteActive(false);
  }, [currentText]);

  return (
    <li className="py-[15px] border-[#e4e4e4] border-b-[1px] border-solid mb-[15px] last:mb-0">
      <div className="flex">
        <div>
          <img
            src={`http://54.93.83.177:80/${product.img_url}`}
            alt="Product image"
            className="max-w-[45px]"
          />
        </div>
        <div className="pl-[10px] pr-[35px] text-[16px] font-bold leading-6 flex-1">
          <span className="inline-block text-navy mb-2">{product.name}</span>

          <div className=" h-[30px] flex mb-2 relative">
            <button
              className={`${styles["change-quality-btn"]} ${styles["change-quality-btn__decrease"]}`}
              onClick={handleDecreaseQuantity}
              disabled={product.quantity === 1}
            >
              <span>-</span>
            </button>
            <span className="w-[45px] flex justify-center items-center text-gray text-[14px] leading-[21px] p-1 border border-[#e4e4e4]">
              {product.quantity}
            </span>
            <button
              className={`${styles["change-quality-btn"]} ${styles["change-quality-btn__increase"]}`}
              onClick={handleIncreaseQuantity}
            >
              <span>+</span>
            </button>
            <span
              className="inline-block self-center text-[#167f92] ml-[15px] cursor-pointer hover:text-red transition-all"
              onClick={handleOpenNote}
            >
              <BsFillPencilFill />
            </span>
            <span
              className="absolute right-[-25px] translate-x-1/2 translate-y-1/2 inline-block text-[18px] text-[#167f92] cursor-pointer hover:text-red transition-all"
              onClick={handleDeleteProduct}
            >
              <BsFillTrashFill />
            </span>
          </div>

          <div className="text-navy text-[16px] leading-[24px] mb-[5px]">
            {product.totalPrice.toFixed(2)} €
          </div>

          {note && (
            <div className="text-[12px] text-navy font-normal italic mb-[5px]">
              {note}
            </div>
          )}

          {noteActive && (
            <div>
              <fieldset className="m-0 p-0 border-red">
                <legend className="text-[15px] ml-[10px] leading-[23px] px-1 text-[#167f92] font-normal">
                  Add note
                </legend>
                <textarea
                  className="text-[14px] leading-[21px] text-gray font-normal w-full px-[10px] resize-none outline-none placeholder:opacity-60"
                  maxLength={160}
                  cols={0}
                  rows={3}
                  placeholder='E.g. "Without onions"'
                  value={currentText}
                  onChange={(e) => setCurrentText(e.target.value)}
                ></textarea>
              </fieldset>
              <div className="mt-[5px] flex justify-end">
                {note ? (
                  <span
                    className="cursor-pointer text-[15px] leading-[24px] text-navy"
                    onClick={handleDeleteNote}
                  >
                    Delete
                  </span>
                ) : (
                  <span
                    className="cursor-pointer text-[15px] leading-[24px] text-navy"
                    onClick={() => setNoteActive(false)}
                  >
                    Cancel
                  </span>
                )}

                <span
                  className="cursor-pointer text-[15px] leading-[24px] text-navy ml-[40px]"
                  onClick={handleSaveNote}
                >
                  Save
                </span>
              </div>
            </div>
          )}
        </div>
      </div>
    </li>
  );
};

export default CartItem;
