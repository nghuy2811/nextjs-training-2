import React, { useCallback, useState } from "react";
import { AiOutlineDropbox, AiOutlineClose } from "react-icons/ai";
import { BsFillCartFill } from "react-icons/bs";

import { useAppSelector } from "../../app/hooks";
import CartData from "./CartData";
import { selectAllProductsArray } from "../../features/products/productsSlice";
import styles from "./style.module.scss";

type ShoppingCartProps = {
  minOrder: number;
};

const ShoppingCart = ({ minOrder }: ShoppingCartProps) => {
  const products = useAppSelector(selectAllProductsArray);
  const [total, setTotal] = useState(0);
  const [openCart, setOpenCart] = useState(false);

  const getTotalPrice = useCallback(
    (total: number) => {
      setTotal(total);
    },
    [total]
  );

  return (
    <>
      <div className={styles["cart-box"]} onClick={() => setOpenCart(true)}>
        <div className="relative bg-[#c00a27] w-full h-full rounded-[6px] flex justify-center items-center">
          <span className="absolute text-[20px] translate-y-[-50%] top-1/2 left-[20px] text-white">
            <BsFillCartFill />
            <span className="absolute left-[15px] top-[-5px] text-[12px] leading-[17px] bg-blue rounded-[50%] w-[17px] h-[17px] text-center">
              {products.length}
            </span>
          </span>
          <div className="text-white text-[17px] leading-[26px] font-bold">
            Cart ({total.toFixed(2)} €)
          </div>
        </div>
      </div>
      <section
        className={styles["shopping-cart"]}
        style={openCart ? { display: "block" } : {}}
      >
        <h4 className="block relative m-0 py-[20px] text-[18px] leading-[25px]  lg:text-[20px] lg:leading-[28px] bg-[#f8f5f2] text-navy font-bold text-center">
          SHOPPING CART
          <span
            className="absolute top-1/2 right-[15px] translate-y-[-50%] lg:hidden cursor-pointer inline-block text-[20px]"
            onClick={() => setOpenCart(false)}
          >
            <AiOutlineClose />
          </span>
        </h4>
        {products.length > 0 ? (
          <CartData minOrder={minOrder} callback={getTotalPrice} />
        ) : (
          <>
            <div className="p-[30px] text-center text-gray">
              <span className="inline-block text-[50px] mb-[20px]">
                <AiOutlineDropbox />
              </span>
              <p className="text-[16px] leading-[24px] mb-[10px]">
                Cart is empty
              </p>
              <p className="text-[16px] leading-[24px] mb-[10px]">
                Start to add some tasty food from the menu and order your food.
              </p>
            </div>
          </>
        )}
      </section>
    </>
  );
};

export default ShoppingCart;
