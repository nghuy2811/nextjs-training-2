import React, { useCallback, useState } from "react";

import Category from "../category/Category";
import Product from "../products/Products";
import styles from "./style.module.scss";

type MainProductsProps = {
  categories: categoryDataType[];
  products: productDataType[];
};

export type ProductsByCategory = {
  [code: string]: productDataType[];
};

const MainProducts = ({ categories, products }: MainProductsProps) => {
  const initialProducts: ProductsByCategory = {};
  const [activeCategory, setActiveCategory] = useState<string>();
  const [selectedCategory, setSelectedCategory] = useState<string>();

  const handleActiveChange = useCallback(
    (code: string) => {
      setActiveCategory(code);
    },
    [activeCategory]
  );

  const handleSelectCategory = useCallback(
    (code: string) => {
      setSelectedCategory(code);
      setActiveCategory(code);
    },
    [selectedCategory, activeCategory]
  );

  const productsByCategory = products.reduce((acc, category) => {
    (acc[category["category_code"]] =
      acc[category["category_code"]] || []).push(category);
    return acc;
  }, initialProducts);

  return (
    <section className={styles["main-products"] + " my-[35px] relative"}>
      <Category
        categories={categories}
        activeCategory={activeCategory}
        callback={handleSelectCategory}
      />
      <Product
        categoryInView={selectedCategory}
        activeCategory={activeCategory}
        onScrollCallback={handleActiveChange}
        categories={categories}
        productsByCategory={productsByCategory}
      />
    </section>
  );
};

export default MainProducts;
